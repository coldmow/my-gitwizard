var pageNR = 0;

var knopVolgende	= document.querySelector("[data-load-next]");
var knopVorige		= document.querySelector("[data-load-previous]");

var display 		= document.querySelector(".container .display");
	
var AR = new XMLHttpRequest();

var nextOrPrev = "init";

AR.open("GET", "step"+pageNR+".html");
AR.send();


knopVolgende.addEventListener("click", runAjaxNext);
knopVorige.addEventListener("click", runAjaxPrevious);


AR.addEventListener("load", ajaxLoad);
AR.addEventListener("error", ajaxError);



function runAjaxNext(){
	pageNR++;
	nextOrPrev = "next";
	AR.open("GET", "step"+pageNR+".html");
	AR.send();
}

function runAjaxPrevious(){
	pageNR--;
	nextOrPrev = "prev";
	AR.open("GET", "step"+pageNR+".html");
	AR.send();
}

function ajaxError(){
	console.log("ERROR");
}

function ajaxLoad(){
	console.log(AR.status);
	if (this.status != 200){
		if (nextOrPrev == "next") {
			pageNR--;
			console.log(pageNR);
		}
		else if (nextOrPrev == "prev") {
			pageNR++;
			console.log(pageNR);
		}
	}else{
		if (nextOrPrev == "next") {
			display.innerHTML = this.responseText;
			console.log(pageNR);
		}
		else if (nextOrPrev == "prev") {
			display.innerHTML = this.responseText;
			console.log(pageNR);
		}
		else if (nextOrPrev == "init") {
			display.innerHTML = this.responseText;
			console.log(pageNR);
		}
		else {
			console.log(pageNR);
		}
	}
}

//-----------------------------------------------------------------------------------
/*
	document.querySelector('[data-step="'+ pageNR +'"]').style.display = "none";
	var nextpage = pageNR+1;
	document.querySelector('[data-step="'+ nextpage +'"]').style.display = "block";
	pageNR++;*/

//-----------------------------------------------------------------------------------

/*	var pagina = document.getAttribute('data-step');
	// var a = document.querySelector('[data-step="'+pagina+'"]')

	switch(pagina) {
	    case 0:
        break;

	    case 1:
	        document.querySelector('[data-step="'+pagina+'"]').style.display = "block";
        break;

	    case 2:
        break;

	    case 3:
        break;

	    case 4:
        break;
	        
	}*/



/*

Routing system:

function processAjaxData(response, urlPath){
    document.getElementById("content").innerHTML = response.html;
    document.title = response.pageTitle;
    window.history.pushState({"html":response.html,"pageTitle":response.pageTitle},"", urlPath);
}

window.onpopstate = function(e){
    if(e.state){
        document.getElementById("content").innerHTML = e.state.html;
        document.title = e.state.pageTitle;
    }
};

 */